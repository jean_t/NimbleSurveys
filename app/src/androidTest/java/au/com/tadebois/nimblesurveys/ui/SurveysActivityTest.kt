package au.com.tadebois.nimblesurveys.ui

import android.support.test.espresso.Espresso
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.app.ActionBar
import android.view.View
import au.com.tadebois.nimblesurveys.R
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by Jean Tadebois.
 */
@RunWith(AndroidJUnit4::class)
class SurveysActivityTest {

    @get:Rule
    var activityRule = ActivityTestRule<SurveysActivity>(SurveysActivity::class.java)

    lateinit var activity: SurveysActivity

    @Before
    fun setup() {
        activity = activityRule.activity
    }

    @Test
    fun shouldForceActivityToRTL() {
        Assert.assertEquals(View.LAYOUT_DIRECTION_RTL, activity.window.decorView.layoutDirection)
    }

    @Test
    fun shouldSetupActionBar() {
        val actionBar = activity.supportActionBar

        Assert.assertNotNull(actionBar)
        Assert.assertTrue(actionBar!!.displayOptions and ActionBar.DISPLAY_HOME_AS_UP == ActionBar.DISPLAY_HOME_AS_UP)
    }

    @Test
    fun shouldRefreshList() {
        val componentIdlingResource = activity.loadingIdlingResourceWrapper.countingIdlingResource
        IdlingRegistry.getInstance().register(componentIdlingResource)

        val actionMenuItemView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.action_refresh), ViewMatchers.withContentDescription(activity.getString(R.string.action_refresh)),
                ViewMatchers.isDisplayed()
            )
        )
        actionMenuItemView.perform(ViewActions.click())

        val appCompatButton = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.take_survey), ViewMatchers.withText(activity.getString(R.string.take_the_survey)),
                ViewMatchers.isDisplayed()
            )
        )
        appCompatButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        IdlingRegistry.getInstance().unregister(componentIdlingResource)
    }

    @Test
    fun shouldOpenQuestionsActivityWhenTakeSurvey() {
        val componentIdlingResource = activity.loadingIdlingResourceWrapper.countingIdlingResource
        IdlingRegistry.getInstance().register(componentIdlingResource)

        val appCompatButton = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.take_survey), ViewMatchers.withText(activity.getString(R.string.take_the_survey)),
                ViewMatchers.isDisplayed()
            )
        )
        appCompatButton.perform(ViewActions.click())

        val viewGroup = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.questions_layout),
                ViewMatchers.isDisplayed()
            )
        )
        viewGroup.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        IdlingRegistry.getInstance().unregister(componentIdlingResource)
    }
}