package au.com.tadebois.nimblesurveys.repository

import android.arch.lifecycle.Observer
import android.arch.paging.PageKeyedDataSource
import android.support.test.InstrumentationRegistry
import au.com.tadebois.nimblesurveys.backend.api.SurveyApi
import au.com.tadebois.nimblesurveys.di.component.DaggerSurveyRepositoryComponent
import au.com.tadebois.nimblesurveys.model.Survey
import au.com.tadebois.nimblesurveys.repository.api.ISurveyRepository
import au.com.tadebois.nimblesurveys.repository.api.SurveyRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.CountDownLatch


/**
 * Created by Jean Tadebois.
 */
class SurveyDataSourceTest {

    @Mock
    lateinit var mockedInitialCallback: PageKeyedDataSource.LoadInitialCallback<Int, Survey>

    @Mock
    lateinit var mockedLoadCallback: PageKeyedDataSource.LoadCallback<Int, Survey>

    private lateinit var surveyRepository: ISurveyRepository

    private lateinit var surveyDataSource: SurveyDataSource

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        surveyRepository = DaggerSurveyRepositoryComponent
            .builder()
            .context(InstrumentationRegistry.getTargetContext())
            .build()
            .getSurveyRepository()

        surveyDataSource = SurveyDataSource(surveyRepository)
    }

    @Test
    fun shouldInvokeCallbackWhenLoadInitial() {
        val observer = TestObserver<State>(2)
        surveyDataSource.state.observeForever(
            observer
        )

        val params = PageKeyedDataSource
            .LoadInitialParams<Int>(
                5,
                false
            )

        val initialCallback = object : PageKeyedDataSource.LoadInitialCallback<Int, Survey>() {
            override fun onResult(
                surveys: List<Survey>,
                position: Int,
                totalCount: Int,
                previousPageKey: Int?,
                nextPageKey: Int?
            ) {

            }

            override fun onResult(surveys: List<Survey>, previousPageKey: Int?, nextPageKey: Int?) {
                Assert.assertTrue(surveys.count() == 5)
                Assert.assertNull(previousPageKey)
                Assert.assertEquals(SurveyDataSource.SECOND_PAGE, nextPageKey)
            }
        }

        surveyDataSource.loadInitial(params, initialCallback)

        observer.await()
        observer.assertResults(Arrays.asList(State.LOADING_INITIAL, State.DONE_INITIAL))
    }

    @Test
    fun shouldErrorWhenLoadInitialFails() {
        surveyRepository = SurveyRepository(
            Retrofit
                .Builder()
                .baseUrl(SurveyApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SurveyApi::class.java)
        )
        surveyDataSource = SurveyDataSource(surveyRepository)

        val observer = TestObserver<State>(2)
        surveyDataSource.state.observeForever(
            observer
        )

        val params = PageKeyedDataSource
            .LoadInitialParams<Int>(
                5,
                false
            )
        surveyDataSource.loadInitial(params, mockedInitialCallback)

        observer.await()
        observer.assertResults(Arrays.asList(State.LOADING_INITIAL, State.ERROR))
    }


    @Test
    fun shouldInvokeCallbackWhenLoadAfter() {
        val observer = TestObserver<State>(2)
        surveyDataSource.state.observeForever(
            observer
        )

        val params = PageKeyedDataSource
            .LoadParams<Int>(
                2,
                5
            )

        val loadCallback = object : PageKeyedDataSource.LoadCallback<Int, Survey>() {
            override fun onResult(surveys: List<Survey>, adjacentPageKey: Int?) {
                Assert.assertTrue(surveys.count() == 5)
                Assert.assertEquals(3, adjacentPageKey)
            }
        }

        surveyDataSource.loadAfter(params, loadCallback)

        observer.await()
        observer.assertResults(Arrays.asList(State.LOADING, State.DONE))
    }

    @Test
    fun shouldErrorWhenLoadAfterFails() {
        surveyRepository = SurveyRepository(
            Retrofit
                .Builder()
                .baseUrl(SurveyApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SurveyApi::class.java)
        )
        surveyDataSource = SurveyDataSource(surveyRepository)

        val observer = TestObserver<State>(2)
        surveyDataSource.state.observeForever(
            observer
        )

        val params = PageKeyedDataSource
            .LoadParams<Int>(
                1,
                5
            )
        surveyDataSource.loadAfter(params, mockedLoadCallback)

        observer.await()
        observer.assertResults(Arrays.asList(State.LOADING, State.ERROR))
    }

    private class TestObserver<T>(expectedCount: Int) : Observer<T> {

        private val _results = ArrayList<T>()
        private val _latch: CountDownLatch = CountDownLatch(expectedCount)

        override fun onChanged(t: T?) {
            if (t != null) {
                _results.add(t)
                _latch.countDown()
            }
        }

        @Throws(InterruptedException::class)
        fun await() {
            _latch.await()
        }

        fun assertResults(expected: List<T>) {
            Assert.assertEquals(expected.size.toLong(), _results.size.toLong())

            for (i in _results.indices) {
                Assert.assertEquals(_results[i], expected[i])
            }
        }
    }
}