package au.com.tadebois.nimblesurveys.repository.api

import android.support.test.InstrumentationRegistry
import au.com.tadebois.nimblesurveys.backend.OAuthToken
import au.com.tadebois.nimblesurveys.backend.api.TokenApi
import au.com.tadebois.nimblesurveys.di.component.DaggerSurveyRepositoryComponent
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by Jean Tadebois.
 */
class TokenRepositoryTest {

    lateinit var tokenRepository: ITokenRepository

    @Before
    fun setup() {
        tokenRepository = DaggerSurveyRepositoryComponent
            .builder()
            .context(InstrumentationRegistry.getTargetContext())
            .build()
            .getTokenRepository()
    }

    @Test
    fun shouldGetToken() {
        val oAuthToken: OAuthToken? = tokenRepository
            .postCredentials(
                TokenApi.GRANT_TYPE,
                TokenApi.USERNAME,
                TokenApi.PASSWORD
            )
            .execute()
            .body()
        val token = oAuthToken?.accessToken

        Assert.assertNotNull(token)
        Assert.assertTrue(token!!.isNotEmpty())
    }
}
