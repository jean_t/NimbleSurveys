package au.com.tadebois.nimblesurveys.repository.api

import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.test.InstrumentationRegistry
import au.com.tadebois.nimblesurveys.backend.SurveyApiAuthenticator
import au.com.tadebois.nimblesurveys.backend.SurveyApiInterceptor
import au.com.tadebois.nimblesurveys.backend.api.SurveyApi
import au.com.tadebois.nimblesurveys.backend.api.TokenApi
import au.com.tadebois.nimblesurveys.di.component.DaggerSurveyRepositoryComponent
import au.com.tadebois.nimblesurveys.model.Survey
import au.com.tadebois.nimblesurveys.repository.local.OAuthTokenRepository
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Jean Tadebois.
 */
class SurveyRepositoryTest {
    lateinit var surveyRepository: ISurveyRepository

    @Before
    fun setup() {
        surveyRepository = DaggerSurveyRepositoryComponent
            .builder()
            .context(InstrumentationRegistry.getTargetContext())
            .build()
            .getSurveyRepository()
    }

    @Test
    fun shouldGetSurveys() {
        val surveys: List<Survey>? = surveyRepository
            .getSurveys()
            .execute()
            .body()

        Assert.assertNotNull(surveys)
        Assert.assertTrue(surveys?.count()!! > 0)
    }

    @Test
    fun shouldGetSurveysPaged() {
        val surveys: List<Survey>? = surveyRepository
            .getSurveys(1, 5)
            .execute()
            .body()
        Assert.assertNotNull(surveys)
        Assert.assertTrue(surveys?.count() == 5)
    }

    @Test
    fun shouldRefreshTokenIfExpired() {
        val sharedPreferences: SharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext())


        val okHttpClientBuilder = OkHttpClient
            .Builder()
            .addInterceptor(
                SurveyApiInterceptor(
                    OAuthTokenRepository(
                        sharedPreferences
                    )
                )
            )
            .authenticator(
                SurveyApiAuthenticator(
                    TokenRepository(
                        Retrofit
                            .Builder()
                            .baseUrl(SurveyApi.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                            .create(TokenApi::class.java)
                    ),
                    OAuthTokenRepository(
                        sharedPreferences
                    )
                )
            )

        surveyRepository = SurveyRepository(
            Retrofit
                .Builder()
                .baseUrl(SurveyApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
                .create(SurveyApi::class.java)
        )

        val surveys: List<Survey>? = surveyRepository
            .getSurveys()
            .execute()
            .body()

        Assert.assertNotNull(surveys)
        Assert.assertTrue(surveys?.count()!! > 0)
    }
}