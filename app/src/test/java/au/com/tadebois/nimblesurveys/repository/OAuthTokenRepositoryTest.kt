package au.com.tadebois.nimblesurveys.repository

import android.content.Context
import android.content.SharedPreferences
import au.com.tadebois.nimblesurveys.backend.OAuthToken
import au.com.tadebois.nimblesurveys.repository.local.IOAuthTokenRepository
import au.com.tadebois.nimblesurveys.repository.local.OAuthTokenRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

/**
 * Created by Jean Tadebois.
 */
@RunWith(RobolectricTestRunner::class)
class OAuthTokenRepositoryTest {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var oAuthTokenRepository: IOAuthTokenRepository

    @Before
    fun setup() {
        sharedPreferences = Mockito.mock(SharedPreferences::class.java)

        oAuthTokenRepository = OAuthTokenRepository(sharedPreferences)
    }

    @Test
    fun shouldBuildTokenFromSharedPreferences() {
        Mockito.`when`(sharedPreferences.getString(OAuthToken.ACCESS_TOKEN, ""))
            .thenReturn("d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93")
        Mockito.`when`(sharedPreferences.getString(OAuthToken.TOKEN_TYPE, ""))
            .thenReturn("bearer")

        val oAuthToken = oAuthTokenRepository.getToken()

        Assert.assertEquals(
            "bearer d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93",
            oAuthToken.getAuthorization()
        )
        verify(sharedPreferences, times(2)).getString(anyString(), anyString())
    }

    @Test
    fun shouldSaveTokenInSharedPreferences() {
        sharedPreferences = RuntimeEnvironment.application.getSharedPreferences("test", Context.MODE_PRIVATE)
        oAuthTokenRepository = OAuthTokenRepository(sharedPreferences)

        oAuthTokenRepository.saveToken(
            OAuthToken(
                "d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93",
                "bearer"
            )
        )

        val accessToken = sharedPreferences.getString(OAuthToken.ACCESS_TOKEN, "")
        Assert.assertEquals("d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93", accessToken)
        val tokenType = sharedPreferences.getString(OAuthToken.TOKEN_TYPE, "")
        Assert.assertEquals("bearer", tokenType)
    }
}