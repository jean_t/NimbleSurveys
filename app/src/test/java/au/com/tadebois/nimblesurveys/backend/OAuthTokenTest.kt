package au.com.tadebois.nimblesurveys.backend

import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by Jean Tadebois.
 */
class OAuthTokenTest {

    lateinit var oauthToken: IOAuthToken

    @Before
    fun setup() {
        oauthToken = OAuthToken(
            "d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93",
            "bearer"
        )
    }

    @Test
    fun shouldReturnAuthorization() {
        val authorization: String = oauthToken.getAuthorization()

        Assert.assertEquals(
            "bearer d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93",
            authorization
        )
    }
}