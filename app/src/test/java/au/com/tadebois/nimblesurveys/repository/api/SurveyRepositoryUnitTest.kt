package au.com.tadebois.nimblesurveys.repository.api

import au.com.tadebois.nimblesurveys.backend.api.SurveyApi
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Jean Tadebois.
 */
@RunWith(MockitoJUnitRunner::class)
class SurveyRepositoryUnitTest {

    @Mock
    lateinit var surveyApi: SurveyApi

    @InjectMocks
    lateinit var surveyRepository: SurveyRepository

    @Test
    fun shouldCallGetSurveysOnSurveyApi() {
        surveyRepository.getSurveys()

        verify(surveyApi, times(1)).getSurveys()
    }

    @Test
    fun shouldCallGetSurveysPagedOnSurveyApi() {
        surveyRepository.getSurveys(1, 10)

        verify(surveyApi, times(1)).getSurveys(1, 10)
    }
}