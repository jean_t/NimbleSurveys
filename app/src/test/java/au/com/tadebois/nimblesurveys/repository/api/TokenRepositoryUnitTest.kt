package au.com.tadebois.nimblesurveys.repository.api

import au.com.tadebois.nimblesurveys.backend.api.TokenApi
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Jean Tadebois.
 */
@RunWith(MockitoJUnitRunner::class)
class TokenRepositoryUnitTest {
    @Mock
    lateinit var tokenApi: TokenApi

    @InjectMocks
    lateinit var tokenRepository: TokenRepository

    @Test
    fun shouldCallPostCredentialsOnTokenApi() {
        tokenRepository.postCredentials(TokenApi.GRANT_TYPE, TokenApi.USERNAME, TokenApi.PASSWORD)

        Mockito.verify(tokenApi, Mockito.times(1))
            .postCredentials(TokenApi.GRANT_TYPE, TokenApi.USERNAME, TokenApi.PASSWORD)
    }
}