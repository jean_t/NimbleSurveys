package au.com.tadebois.nimblesurveys.model

import android.os.Bundle
import au.com.tadebois.nimblesurveys.model.Survey
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

/**
 * Created by Jean Tadebois.
 */
@RunWith(RobolectricTestRunner::class)
class SurveyTest {
    @Test
    fun shouldBeParcelable() {
        val bundle = Bundle()
        bundle.putParcelable(Survey.SURVEY_KEY, Survey("d5de6a8f8f5f1cfe51bc", "Title", "Description", "http://image.jpg"))

        val survey: Survey? = bundle.getParcelable(Survey.SURVEY_KEY)

        Assert.assertNotNull(survey)
        Assert.assertEquals("Title", survey?.title)
        Assert.assertEquals("Description", survey?.description)
        Assert.assertEquals("http://image.jpg", survey?.coverImageUrl)
    }

    @Test
    fun shouldBeParcelableInAList() {
        val surveyList = ArrayList<Survey>()
        surveyList.add(Survey("d5de6a8f8f5f1cfe51bc", "Title", "Description", "http://image.jpg"))
        val bundle = Bundle()
        bundle.putParcelableArrayList(Survey.SURVEY_LIST_KEY, surveyList)

        val list = bundle.getParcelableArrayList<Survey>(Survey.SURVEY_LIST_KEY)

        Assert.assertNotNull(list)
        Assert.assertEquals("Title", surveyList[0].title)
        Assert.assertEquals("Description", surveyList[0].description)
        Assert.assertEquals("http://image.jpg", surveyList[0].coverImageUrl)
        Assert.assertEquals(1, surveyList.size)
    }
}