package au.com.tadebois.nimblesurveys.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import au.com.tadebois.nimblesurveys.R
import au.com.tadebois.nimblesurveys.databinding.ActivitySurveysBinding
import au.com.tadebois.nimblesurveys.model.Survey
import au.com.tadebois.nimblesurveys.repository.State
import au.com.tadebois.nimblesurveys.util.CountingIdlingResourceWrapper
import au.com.tadebois.nimblesurveys.util.OnTakeSurveyClickListener
import au.com.tadebois.nimblesurveys.viewmodel.SurveyListViewModel
import kotlinx.android.synthetic.main.activity_surveys.*


class SurveysActivity : AppCompatActivity(), OnTakeSurveyClickListener {

    lateinit var loadingIdlingResourceWrapper: CountingIdlingResourceWrapper

    private lateinit var viewDataBinding: ActivitySurveysBinding
    private lateinit var viewModel: SurveyListViewModel
    private lateinit var surveyListAdapter: SurveyListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_surveys)
        viewModel = ViewModelProviders.of(this)
            .get(SurveyListViewModel::class.java)
        viewDataBinding.viewmodel = viewModel
        loadingIdlingResourceWrapper = viewModel.loadingIdlingResourceWrapper

        initAdapter()
        initState()
        setupActionBar()
        forceRTLIfSupported()
    }

    private fun initAdapter() {
        recyclerView.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )

        surveyListAdapter = SurveyListAdapter(this)
        recyclerView.adapter = surveyListAdapter
        viewModel.surveyList.observe(this, Observer {
            surveyListAdapter.submitList(it)
        })

        PagerSnapHelper().attachToRecyclerView(recyclerView)
        recyclerViewIndicator.setRecyclerView(recyclerView)
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            when (state) {
                State.DONE_INITIAL -> recyclerViewIndicator.forceUpdateItemCount()
                State.DONE -> recyclerViewIndicator.forceUpdateItemCount()
                State.ERROR -> Toast
                    .makeText(
                        applicationContext,
                        getString(R.string.network_error),
                        Toast.LENGTH_LONG
                    ).show()
            }
        })
    }

    private fun setupActionBar() {
        setSupportActionBar(findViewById(R.id.my_toolbar))
        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }
    }

    private fun forceRTLIfSupported() {
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                viewModel.refresh()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onTakeSurveyClick(survey: Survey) {
        startActivity(
            Intent(this, QuestionsActivity::class.java)
                .putExtra(Survey.SURVEY_KEY, survey)
        )
    }
}