package au.com.tadebois.nimblesurveys.util

import au.com.tadebois.nimblesurveys.model.Survey

/**
 * Created by Jean Tadebois.
 */
interface OnTakeSurveyClickListener {
    fun onTakeSurveyClick(survey: Survey)
}