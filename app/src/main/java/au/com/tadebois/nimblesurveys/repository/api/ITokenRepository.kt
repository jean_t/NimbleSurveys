package au.com.tadebois.nimblesurveys.repository.api

import au.com.tadebois.nimblesurveys.backend.OAuthToken
import retrofit2.Call

/**
 * Created by Jean Tadebois.
 */
interface ITokenRepository {
    fun postCredentials(grantType: String, username: String, password: String): Call<OAuthToken>
}