package au.com.tadebois.nimblesurveys.repository.local

import au.com.tadebois.nimblesurveys.backend.OAuthToken

/**
 * Created by Jean Tadebois.
 */
interface IOAuthTokenRepository {
    fun getToken(): OAuthToken

    fun saveToken(oAuthToken: OAuthToken)
}