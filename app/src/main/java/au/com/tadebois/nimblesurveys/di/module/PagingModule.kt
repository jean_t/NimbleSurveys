package au.com.tadebois.nimblesurveys.di.module

import android.arch.paging.PagedList
import au.com.tadebois.nimblesurveys.repository.api.ISurveyRepository
import au.com.tadebois.nimblesurveys.repository.SurveyDataSource
import au.com.tadebois.nimblesurveys.repository.SurveyDataSourceFactory
import au.com.tadebois.nimblesurveys.backend.api.SurveyApi
import dagger.Module
import dagger.Provides

/**
 * Created by Jean Tadebois.
 */
@Module
class PagingModule {
    @Provides
    fun providesSurveyDataSource(surveyRepository: ISurveyRepository): SurveyDataSource =
        SurveyDataSource(surveyRepository)

    @Provides
    fun providesSurveyDataSourceFactory(surveyRepository: ISurveyRepository): SurveyDataSourceFactory =
        SurveyDataSourceFactory(surveyRepository)

    @Provides
    fun providesPagedListConfig(): PagedList.Config {
        return PagedList.Config.Builder()
            .setPageSize(SurveyApi.PAGE_SIZE)
            .setInitialLoadSizeHint(SurveyApi.PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
    }
}