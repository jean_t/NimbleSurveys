package au.com.tadebois.nimblesurveys.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import au.com.tadebois.nimblesurveys.model.Survey
import au.com.tadebois.nimblesurveys.repository.api.ISurveyRepository
import javax.inject.Inject

/**
 * Created by Jean Tadebois.
 */
class SurveyDataSourceFactory @Inject constructor(
    private val surveyRepository: ISurveyRepository
) : DataSource.Factory<Int, Survey>() {

    val surveyDataSourceLiveData = MutableLiveData<SurveyDataSource>()

    override fun create(): DataSource<Int, Survey> {
        val newsDataSource = SurveyDataSource(surveyRepository)
        surveyDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}