package au.com.tadebois.nimblesurveys.di.component

import android.app.Application
import android.arch.paging.PagedList
import au.com.tadebois.nimblesurveys.App
import au.com.tadebois.nimblesurveys.repository.SurveyDataSourceFactory
import au.com.tadebois.nimblesurveys.di.module.ActivityBindings
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by Jean Tadebois.
 */
@Singleton
@Component(modules = [ActivityBindings::class], dependencies = [SurveyRepositoryComponent::class])
interface AppComponent : AndroidInjector<App> {
    fun inject(application: Application)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(application: Application): Builder

        fun surveyApiComponent(surveyRepositoryComponent: SurveyRepositoryComponent): Builder

        fun build(): AppComponent
    }

    fun surveyDataSourceFactory(): SurveyDataSourceFactory

    fun pagedListConfig(): PagedList.Config
}
