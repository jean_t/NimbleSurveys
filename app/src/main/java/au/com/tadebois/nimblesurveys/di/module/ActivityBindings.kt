package au.com.tadebois.nimblesurveys.di.module

import au.com.tadebois.nimblesurveys.ui.SurveysActivity
import au.com.tadebois.nimblesurveys.di.scope.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Jean Tadebois.
 */
@Module
abstract class ActivityBindings {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): SurveysActivity
}