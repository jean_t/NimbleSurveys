package au.com.tadebois.nimblesurveys.di.scope

import javax.inject.Scope

/**
 * Created by Jean Tadebois.
 */
@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class ActivityScope