package au.com.tadebois.nimblesurveys.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.databinding.ObservableBoolean
import au.com.tadebois.nimblesurveys.App
import au.com.tadebois.nimblesurveys.model.Survey
import au.com.tadebois.nimblesurveys.repository.State
import au.com.tadebois.nimblesurveys.repository.SurveyDataSource
import au.com.tadebois.nimblesurveys.repository.SurveyDataSourceFactory
import au.com.tadebois.nimblesurveys.util.CountingIdlingResourceWrapper

/**
 * Created by Jean Tadebois.
 */
class SurveyListViewModel(application: Application) : AndroidViewModel(application) {
    private val surveyDataSourceFactory: SurveyDataSourceFactory =
        (application as App).appComponent.surveyDataSourceFactory()

    private val pagedConfig: PagedList.Config = (application as App).appComponent.pagedListConfig()

    var loading: ObservableBoolean = ObservableBoolean(false)
    var loadingIdlingResourceWrapper = CountingIdlingResourceWrapper("loading")

    var surveyList: LiveData<PagedList<Survey>> = LivePagedListBuilder<Int, Survey>(
        this.surveyDataSourceFactory,
        pagedConfig
    ).build()

    fun refresh() {
        surveyDataSourceFactory.surveyDataSourceLiveData.value?.invalidate()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<SurveyDataSource,
            State>(surveyDataSourceFactory.surveyDataSourceLiveData, SurveyDataSource::state)

    init {
        getState().observeForever {
            when (it) {
                State.DONE_INITIAL -> {
                    loading.set(false)
                    loadingIdlingResourceWrapper.decrement()
                }
                State.LOADING_INITIAL -> {
                    loading.set(true)
                    loadingIdlingResourceWrapper.increment()
                }
            }
        }
    }
}