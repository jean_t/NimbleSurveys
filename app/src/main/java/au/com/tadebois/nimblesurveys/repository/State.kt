package au.com.tadebois.nimblesurveys.repository

enum class State {
    DONE_INITIAL, DONE, LOADING_INITIAL, LOADING, ERROR
}